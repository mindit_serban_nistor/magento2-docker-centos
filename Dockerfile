FROM centos:centos6

# Installing base components
RUN yum -y install wget curl unzip supervisor g++ make mc vim tar gcc pcre-devel openssl-devel patch libmcrypt-devel libxml2-devel bzip2-devel libcurl-devel readline-devel git openssh
RUN yum -y update

# Installing PHP 5.6
RUN yum -y install https://mirror.webtatic.com/yum/el6/latest.rpm
RUN yum -y install php56w-pecl-memcache php56w-fpm php56w-intl php56w-mcrypt php56w-mbstring php56w-mysql php56w-pdo php56w-mbstring php56w-soap php56w-pecl-zendopcache php56w-xml php56w-gd php56w-opcache php56w-pecl-imagick
# <---- End

# Installing MySQL 5.6
RUN rpm -Uvh http://dev.mysql.com/get/mysql-community-release-el6-5.noarch.rpm
RUN yum -y install mysql-community-server
# <---- End

# Installing Nginx
RUN wget http://nginx.org/packages/centos/6/noarch/RPMS/nginx-release-centos-6-0.el6.ngx.noarch.rpm
RUN rpm -ivh nginx-release-centos-6-0.el6.ngx.noarch.rpm
RUN yum -y install nginx
# <---- End

# Installing Varnish
RUN yum -y upgrade ca-certificates
RUN yum -y install https://repo.varnish-cache.org/redhat/varnish-4.0.el6.rpm
RUN yum -y install epel-release
RUN yum -y install varnish
# <---- End

# Installing Composer
RUN curl -sS https://getcomposer.org/installer | php
RUN mv composer.phar /usr/local/bin/composer
# <---- End

# Configuring magento system user
RUN useradd -d /home/magento -g nginx -m -s /bin/bash magento
RUN echo "magento:magento" | chpasswd
RUN mkdir -p /home/magento/.composer /home/magento/.ssh
ADD _root_fs/home/magento/.composer/auth.json /home/magento/.composer/auth.json
ADD _root_fs/home/magento/.ssh/id_rsa /home/magento/.ssh/id_rsa
ADD _root_fs/home/magento/.ssh/id_rsa.pub /home/magento/.ssh/id_rsa.pub
ADD _root_fs/home/magento/.ssh/known_hosts /home/magento/.ssh/known_hosts
RUN chown -R magento.nginx /home/magento/
RUN chmod 600 /home/magento/.composer/auth.json && chmod 700 /home/magento/.ssh && chmod 600 /home/magento/.ssh/id_rsa && chmod 644 /home/magento/.ssh/id_rsa.pub && chmod 644 /home/magento/.ssh/known_hosts
# <---- End

# Checking out Magento 2 from Dufry Employee Store Bitbucket repository
RUN su - magento -c "ssh-agent /bin/bash -c 'ssh-add /home/magento/.ssh/id_rsa; git clone -b master git@bitbucket.org:mindit_serban_nistor/dufry-employee-store.git /home/magento/app'"
# <---- End

# Configuring Dufry Employee Store pseudo-service
ADD _root_fs/etc/init.d/dufry-employee-store /etc/init.d/dufry-employee-store
RUN chmod 755 /etc/init.d/dufry-employee-store
# <---- End

# ----> Configuring system
RUN chown -R magento.nginx /home/magento/app
RUN find /home/magento/app -type d -exec chmod 770 {} \; && find /home/magento/app -type f -exec chmod 660 {} \; && chmod u+x /home/magento/app/bin/magento

# nginx
ADD _root_fs/etc/nginx/conf/fastcgi_params.conf /etc/nginx/conf/fastcgi_params.conf
ADD _root_fs/etc/nginx/conf/magento.conf /etc/nginx/conf/magento.conf
RUN mv /etc/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf.orig
ADD _root_fs/etc/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf

# php
ADD _root_fs/etc/php.ini /etc/php.ini

# php fpm
RUN mv /etc/php-fpm.d/www.conf /etc/php-fpm.d/www.conf.orig
ADD _root_fs/etc/php-fpm.d/www.conf /etc/php-fpm.d/www.conf
RUN mkdir -p /var/lib/php/session
RUN mkdir -p /var/lib/php/wsdlcache
RUN chmod -R 777 /var/lib/php/session
RUN chmod -R 777 /var/lib/php/wsdlcache

# varnish
RUN mv /etc/sysconfig/varnish /etc/sysconfig/default.varnish
ADD _root_fs/etc/sysconfig/varnish /etc/sysconfig/varnish
ADD _root_fs/etc/varnish/default.vcl /etc/varnish/default.vcl

# bash
ADD _root_fs/scripts /scripts
RUN chmod +x -R /scripts

# install magento
RUN /scripts/install.sh
# <---- End
