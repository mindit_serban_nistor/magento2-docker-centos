#!/bin/bash
set -e

/etc/init.d/dufry-employee-store start

mysql -e 'DROP DATABASE IF EXISTS magento;'
mysql -e 'CREATE DATABASE IF NOT EXISTS magento;'

MAGENTO_PATH=/home/magento/app
MAGENTO_CONSOLE=bin/magento

su - magento -c "cd $MAGENTO_PATH && composer install"

COMMANDS=(
    "$MAGENTO_PATH/$MAGENTO_CONSOLE setup:install
        --backend-frontname=admin
        --session-save=db
        --db-host=127.0.0.1
        --db-name=magento
        --db-user=root
        --base-url=http://magento.docker.loc/
        --language=en_US
        --timezone=Europe/Bucharest
        --currency=EUR
        --admin-lastname=Nistor
        --admin-firstname=Serban
        --admin-email=serban.nistor@mindit.ro
        --admin-user=admin
        --admin-password=admin123"
    "$MAGENTO_PATH/$MAGENTO_CONSOLE setup:di:compile"
    )

ELEMENTS=${#COMMANDS[@]}

for (( i=0;i<$ELEMENTS;i++)); do
    echo "${COMMANDS[${i}]}"
    ${COMMANDS[${i}]}
    case "$i" in
    0) echo 'Update Magento configuration'
        mysql -e "INSERT INTO \`magento\`.\`core_config_data\`
        (\`path\`, \`value\`)
        VALUES
        ('dev/template/minify_html', 1),
        ('dev/js/enable_js_bundling', 1),
        ('dev/js/merge_files', 1),
        ('dev/js/minify_files', 1),
        ('dev/css/merge_css_files', 1),
        ('dev/css/minify_files', 1),
        ('web/seo/use_rewrites', 1),
        ('web/url/redirect_to_base', 1),
        ('admin/security/use_form_key', 1)
        ON DUPLICATE KEY UPDATE \`value\` = VALUES(\`value\`);"
    ;;
    esac
done

chown -R magento:nginx $MAGENTO_PATH
chmod -R 777 $MAGENTO_PATH/var $MAGENTO_PATH/pub/static
