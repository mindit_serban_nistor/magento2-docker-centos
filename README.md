# Docker container for Dufry Employee Store (CentOS 6)

## How to build image manually

- In console go to the folder where the Dockerfile resides and execute the following command for building the  Docker image

```
$ docker build -t mindit/magento2:latest .
```

- After build is complete, add virtual host to *hosts* file

```
127.0.0.1 magento.docker.loc
```

- Launch container

```
$ docker run --cap-add=ALL -p 127.0.0.1:80:80 -e VIRTUAL_HOST=magento.docker.loc -i -t mindit/magento2:latest /bin/bash
```

- After container is launched run

```
$ /etc/init.d/dufry-employee-store start
```
